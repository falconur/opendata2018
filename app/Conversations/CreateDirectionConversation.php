<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Attachments\Location;
use Illuminate\Support\Facades\Auth;

class CreateDirectionConversation extends Conversation
{
    public function id()
    {
        $this->bot->userStorage()->save([
            'id' => Auth::user()->id,
        ]);

        $this->askLocationfrom();
    }

    public function askLocationfrom()
    {
        $this->askForLocation("Yo'lga chiqish manzilingizni yuboring", function (Location $location) {
            $this->bot->userStorage()->save([
                'location_from_long' => $location->getLongitude(),
                'location_from_lat' => $location->getLatitude()
            ]);
        });

        $this->askLocationto();
    }

    public function askLocationto()
    {
        $this->askForLocation("Bormoqchi bo'lgan manzilingizni yuboring", function (Location $location) {
            $this->bot->userStorage()->save([
                'location_to_long' => $location->getLongitude(),
                'location_to_lat' => $location->getLatitude()
            ]);
        });

        $this->asktime_from();
    }

    public function asktime_from()
    {
        $this->askForLocation("Yo'lga chiqish vaqtingizni tanlang", function (Location $location) {
            $this->bot->userStorage()->save([
                'location_to_long' => $location->getLongitude(),
                'location_to_lat' => $location->getLatitude()
            ]);
        });
    }
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->id();
    }
}
