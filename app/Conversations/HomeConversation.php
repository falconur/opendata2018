<?php

namespace App\Conversations;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;


class HomeConversation extends Conversation
{
    public function suggest()
    {
        $question = Question::create("Xizmatimizdan foydalanishingiz mumkin:")->addButtons([
            Button::create("Bosh sahifaga o'tish")->value(0),
            Button::create("Yo'nalishlarimni ko'rish")->value(1),
            Button::create("Yangi yo'nalish yaratish")->value(2),
        ]);

        $this->ask($question, function (Answer $response){
            if ($response->isInteractiveMessageReply())
            {
                if ($response->getValue() == 1){
                    $this->bot->startConversation(new ListDirectionsConversation());
                }elseif($response->getValue() == 2){
                    $this->bot->startConversation(new CreateDirectionConversation());
                }elseif ($response->getValue() == 0){
                    $this->bot->startConversation(new HomeConversation());
                }
            }else{
                $this->say('Faqat tugmalarni bosish mumkin!');
                $this->suggest();
            }
        });
    }
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->suggest();
    }
}
