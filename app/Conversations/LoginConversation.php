<?php

namespace App\Conversations;


use App\User;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Psr\Log\LoggerInterface;
use Psy\Command\BufferCommand;
use Symfony\Component\Console\Tests\Command\ListCommandTest;
use BotMan\BotMan\Messages\Attachments\Location;

class LoginConversation extends Conversation
{
    public $user;
    public function askEmail()
    {
        $question = Question::create('Elektron pochtangizni kiriting (example@mail.com):');

        $this->ask($question, function (Answer $response) {

            $validator = \Validator::make(['email' => $response->getText()], [
                'email' => 'email',
            ]);

            if ($validator->fails()) {
                return $this->repeat("Iltimos to'g'ri e-mail kiriting. Yuqoridagi misolga qarang!");
            }

            $this->user = User::where('email', $response->getText())->first();

            if (empty($this->user) == 0){
                $this->askPassword();
            }else{
                $this->say("Bu email ro'yxatdan o'tmagan");
                return $this->bot->startConversation(new StartConversation());
            }
        });
    }

    public function askPassword()
    {
        $question = Question::create('Parolingizni kiriting:');

        $this->ask($question, function (Answer $response) {
            if(Auth::attempt(['email' => $this->user->email, 'password' => $response->getText()])){
                $this->say('Salom, '.Auth::user()->fullname.'. Carpooling tizimiga xush kelibsiz! Profilingizga istalgan tizimdan kirishingiz mumkin!' );
                return $this->bot->startConversation(new HomeConversation());
            }else{
                $this->say("Parol noto'g'ri!");
                return $this->bot->startConversation(new StartConversation());
            }
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askEmail();
    }
}
