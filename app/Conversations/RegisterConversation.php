<?php

namespace App\Conversations;

use App\User;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use Illuminate\Support\Facades\Auth;

class RegisterConversation extends Conversation
{
    public function askName(){
        $question = Question::create('Iltimos ismingizni kiriting:');

        $this->ask($question, function (Answer $response) {
            $this->bot->userStorage()->save([
                'fullname' => $response->getText(),
            ]);

            $this->askEmail();
//            $this->say('Cool - you said ' . $response->getText());
        });

//        $question = Question::create('Tanlag:')->addButtons([
//            Button::create('Tizimga kirish')->value('login'),
//            Button::create("Ro'yxatdan o'tish")->value('register'),
//        ]);
//
//        return $this->ask($question, function (Answer $answer){
//           if ($answer->getValue() == 'login')
//           {
//               $this->say('elektron pochtangisni kiriting');
//           }elseif ($answer->getValue() == 'register')
//           {
//               $this->say('parolingizni kiriting');
//           }
//        });
    }

    public function askEmail()
    {
        $question = Question::create('Elektron pochtangizni kiriting (example@mail.com):');

        $this->ask($question, function (Answer $response) {

            $validator = \Validator::make(['email' => $response->getText()], [
                'email' => 'email',
            ]);

            if ($validator->fails()) {
                return $this->repeat("Iltimos to'g'ri e-mail kiriting. Yuqoridagi misolga qarang!");
            }

            $this->bot->userStorage()->save([
                'email' => $response->getText(),
            ]);

            $this->askMobile();
        });
    }

    public function askMobile()
    {
        $question = Question::create('Telefon raqamingizni kiriting (+998912345678):');

        $this->ask($question, function (Answer $response) {
            $this->bot->userStorage()->save([
                'phone' => $response->getText(),
            ]);

            $this->askGender();
        });
    }

    public function askGender()
    {
        $question = Question::create('Iltimos jinsingizni tanlang. Bu sizga qulayroq xizmat ko\'rsatishimiz uchun kerak:')->addButtons([
            Button::create('Erkak')->value(1),
            Button::create('Ayol')->value(0),
        ]);

        $this->ask($question, function (Answer $response) {
            if ($response->isInteractiveMessageReply() == 0)
            {
                return $this->askGender();
//                $question = Question::create('Jinsingizni kiriting:')->addButtons([
//                    Button::create('Erkak')->value(1),
//                    Button::create('Ayol')->value(0),
//                ]);
//                return $this->repeat($question);
            }

            $this->bot->userStorage()->save([
                'gender' => $response->getValue(),
            ]);

            $this->askPassword();
        });
    }

    public function askPassword()
    {
        $question = Question::create('Parol tanlang:');

        $this->ask($question, function (Answer $response) {
            $this->bot->userStorage()->save([
                'password' => $response->getText(),
            ]);

            $this->tasdiqlash();
        });
    }

    public function tasdiqlash()
    {
        $user = $this->bot->userStorage()->find();
        $gender = $user->get('gender') ? 'Erkak': 'Ayol';
        $message = '
Ma\'lumotlar to\'g\'riligini tasdiqlaysizmi?

-------------------------------------- 
Ism :  '.$user->get('fullname').'
Elektron pochta : ' . $user->get('email').'
Telefon : ' . $user->get('phone').' 
Jins : ' . $gender .'
Parol : ' . $user->get('password').'
---------------------------------------
';
        $question = Question::create($message)->addButtons([
            Button::create('Ha')->value('no'),
            Button::create("Yo'q")->value('yes'),
        ]);

        $this->ask($question, function (Answer $response) {
            if ($response->isInteractiveMessageReply())
            {
                if ($response->getValue() == 'no'){
                    $this->register();
                }elseif ($response->getValue() == 'yes'){
                    return $this->bot->startConversation(new StartConversation());
                }
            }else{
                return $this->tasdiqlash();
            }

//            $this->tasdiqlash();
        });
    }

    public function register()
    {
        $user = $this->bot->userStorage()->find();

        $password = $user->get('password');


        $user = User::create([
            'fullname' => $user->get('fullname'),
            'email' => $user->get('email'),
            'phone' => $user->get('phone'),
            'gender' => $user->get('gender'),
            'password' => bcrypt($password),
            'status' => 1
        ]);
        Auth::attempt(['email' => $user->email, 'password' => $password]);
        $this->say('Salom, '.Auth::user()->fullname.'. Carpooling tizimiga xush kelibsiz! Profilingizga istalgan tizimdan kirishingiz mumkin!' );
        return $this->bot->startConversation(new HomeConversation());
    }
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askName();
    }
}
