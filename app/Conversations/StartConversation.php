<?php

namespace App\Conversations;


use App\Http\Controllers\BotManController;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class StartConversation extends Conversation
{
    public function start()
    {
        $question = Question::create('Tanlang:')->addButtons([
            Button::create('Tizimga kirish')->value('login'),
            Button::create("Ro'yhatdan o'tish")->value('register'),
        ]);

        $this->ask($question, function (Answer $response) {
            if ($response->isInteractiveMessageReply())
            {
                if ($response->getValue() == 'register'){
                    $this->bot->startConversation(new RegisterConversation());
                }elseif($response->getValue() == 'login'){
                    $this->bot->startConversation(new LoginConversation());
                }
            }else{
                $this->say('Faqat tugmalarni bosish mumkin!');
                $this->start();
            }
        });
    }
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->start();
    }
}
