<?php

namespace App\Http\Controllers;

use App\Track;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tracks = Track::where('user_id', Auth::user()->id)
            ->get();

        return view('Track.index', compact('tracks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Track.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        // return $request;        
        $this->validate($request, [
            'time_interval' => 'required|integer',            
            'partner' => 'required|integer',
            'price' => 'required|integer',
        ]);

        Track::create([
            'user_id' => Auth::user()->id,
            'location_from_long' => $request->location_from_long,
            'location_from_lat' => $request->location_from_lat,
            'location_to_long' => $request->location_to_long,
            'location_to_lat' => $request->location_to_lat,
            'time_from' => $request->time_from,
            'time_to' => $request->time_to,
            'time_interval' => $request->time_interval,
            'days' => implode($request->days),
            'partner' => $request->partner,
            'price' => $request->price,
            'status' => 0
        ]);

        

        return redirect()->route('Track.index')
            ->with('success', 'Track added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function show(Track $track)
    {
        return view('Video.show', compact('track'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function edit(Track $track)
    {
        return view('Track.edit', compact('track'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param Track $track
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Track $track)
    {
        $this->validate($request, [
            'location_from_long' => 'required|integer',
            'location_from_lat' => 'required|integer',
            'location_to_long' => 'required|integer',
            'location_to_lat' => 'required|integer',
            'time_from' => 'required|time',
            'time_to' => 'required|time',
            'time_interval' => 'required|integer',
            'days' => 'required|integer',
            'partner' => 'required|integer',
            'price' => 'required|integer',
        ]);

        $track->update($request->all());

        return redirect()->route('Track.index')
            ->with('success', 'Track updated successfully');
    }

    /**
     * @param Track $track
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Track $track)
    {
        $track->delete();

        return redirect()->route('Track.index')
            ->with('success', 'Track deleted successfully');
    }
}
