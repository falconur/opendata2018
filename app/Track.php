<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    protected $fillable = ['user_id', 'location_from_long', 'location_from_lat', 'location_to_long', 'location_to_lat', 
    'time_from', 'time_to', 'time_interval', 'days', 'partner', 'price', 'status'];
}
