<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->double('location_from_long');
            $table->double('location_from_lat');
            $table->double('location_to_long');
            $table->double('location_to_lat');
            $table->time('time_from');
            $table->time('time_to');
            $table->integer('time_interval');
            $table->string('days');
            $table->smallInteger('partner');
            $table->decimal('price');
            $table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracks');
    }
}
