var mymap = L.map('mapid').setView([41.3111, 69.2796], 12);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoibm9vcmV5MTIzIiwiYSI6ImNqcDZhbjFudTAwY2Mza28wNmVlN3Zzb3kifQ.UqRoyV2CAAvL__dmpp1WOA', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',

    id: 'mapbox.streets',
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap)

var newPopup1 = L.popup({
    closeOnClick: false,
    autoClose: false
}).setContent("From");
var newPopup2 = L.popup({
    closeOnClick: false,
    autoClose: false
}).setContent("To");
var marker1 = L.marker([41.3111, 69.30], {
    draggable: true
}).addTo(mymap).bindPopup(newPopup1).openPopup();
var marker2 = L.marker([41.3111, 69.2796], {
    draggable: true
}).addTo(mymap).bindPopup(newPopup2).openPopup();

marker1.on('dragend', function (e) {
    document.getElementById('location_from_lat').value = marker1.getLatLng().lat;
    document.getElementById('location_from_long').value = marker1.getLatLng().lng;
    marker1.bindPopup('From<br>It is Awesome').openPopup();
});
marker2.on('dragend', function (e) {
    document.getElementById('location_to_lat').value = marker2.getLatLng().lat;
    document.getElementById('location_to_long').value = marker2.getLatLng().lng;
    marker2.bindPopup('To<br>It is Awesome').openPopup();
});