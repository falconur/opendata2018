@extends('master.master')
@section('main')
    <header id="header">
        {{--<div class="header-top">--}}
        {{--</div>--}}
        <div class="container-fluid main-menu">
            <div class="row align-items-center justify-content-between d-flex">
                <a href="index.html"><span class="logo">CarpoolUz</span></a>
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li><a href="index.html"><span class="menu-active">Sherik topish</span></a></li>
                        <li><a href="about.html"><span class="menu-active">Mijoz qidirish</span></a></li>
                        <li><a href="service.html"><span class="menu-active">Biz haqimizda</span></a></li>
                        <li><a href="gallery.html"><span class="menu-active">Takliflar</span></a></li>
                        <li><a href="elements.html"><span class="menu-active">Tizimga kirish</span></a></li>
                        <li><a href="contact.html"><span class="menu-active">Ro'yhatdan o'tish</span></a></li>
                    </ul>
                </nav><!-- #nav-menu-container -->
            </div>
        </div>
    </header><!-- #header -->

    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container-fluid">
            <div class="row fullscreen d-flex align-items-center justify-content-between">

                <div class="col-lg-4  col-md-6 header-right" style="margin-top: 400px;margin-left: 200px;">

                    <form class="form">

                        <div class="form-group">

                            <a href="#" class="btn btn-primary btn-lg btn-block text-center text-uppercase">Mijoz qidirish</a>

                        </div>
                    </form>
                </div>
                <div class="col-lg-4  col-md-6 header-right" style="margin-top: 400px;margin-right: 200px;">
                    <form class="form">

                        <div class="form-group">

                            <a href="{{route('main.routing')}}" class="btn btn-primary btn-lg btn-block text-center text-uppercase">Yo'nalish belgilash</a>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start home-about Area -->
    <section class="home-about-area section-gap">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 about-left">
                    <img class="img-fluid" src="template/img/about-img.jpg" alt="">
                </div>
                <div class="col-lg-6 about-right">
                    <h1>Haydovchilar va yo'lovchilarni birlashtiruvchi yirik tizim</h1>
                    <h4>Biz sizga yordam berishdan mamnunmiz</h4>
                    <p>Carpooling xizmati bilan har kungi ishga va o'qishga borishda doimiy sheriklar toping
                        yoki o'z avtomobilingizdagi bo'sh o'rinlarni boshqalar bilan ulashing. 
                        Yo'l noqulayliklaridan qutuling yoki qo'shimcha daromad oling. Bir vaqtning o'zida ham 
                        yo'lovchi ham haydovchilar uchun manfaatli yondashuv. Taksi haydovchilari uchun ham o'z 
                        daromadlarini ko'paytirish uchun ajoyib imkoniyat. 
                        Batafsil Biz haqimizda bo'limida.
                    </p>
                    <a class="text-uppercase primary-btn" href="#">Biz haqimizda</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End home-about Area -->


    <!-- Start services Area -->
    <section class="services-area pb-120">
        <div class="container">
            <div class="row section-title">
                <h1>Biz qanday xizmatlar taklif qilamiz</h1>
                <p>Yangi tanishlar va yangicha taassurotlar</p>
            </div>
            <div class="row">
                <div class="col-lg-4 single-service">
                    <span class="lnr lnr-car"></span>
                    <a href="#"><h4>Yo'lovchilar uchun</h4></a>
                    <p>
                        Har kunlik ishga va o'qishga borishingizda siz bilan bir yo'nalishdagi odamlarni topishda yordam beramiz.
                        Bunda, albatta, yo'lingiz bir o'zingiz taksi xizmatidan foydalangandan arzonroq tushadi.
                    </p>
                </div>
                <div class="col-lg-4 single-service">
                    <span class="lnr lnr-briefcase"></span>
                    <a href="#"><h4>O'z ulovini boshqalar bilan ulashmoqchilar uchun</h4></a>
                    <p>
                        Agar siz doimiy ravishda o'z avtomobilingizda ishga qatnasangiz va bo'sh joylarni boshqa odamlar bilan 
                        ulashishga tayyor bo'lsangiz, bizning tizim sizga mos yo'lovchilar guruhlarini topishda yordam beradi.
                    </p>
                </div>
                <div class="col-lg-4 single-service">
                    <span class="lnr lnr-bus"></span>
                    <a href="#"><h4>Taksi haydovchilari va xizmatlar uchun</h4></a>
                    <p>
                        Bizning tizim orqali doimiy mijozlar toping va har kunlik ishonchli daromadga ega bo'ling
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End services Area -->


    <!-- Start reviews Area -->
    <section class="reviews-area section-gap">
        <div class="container">
            <div class="row section-title">
                <h1>Mijozlar fikrlari</h1>
                <p>Tirbandliklar oldini olishda va tabiatni asrashga o'z hissamizni qo'shamiz</p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-review">
                        <h4></h4>
                        <p>
                            Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.
                        </p>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-review">
                        <h4>Chad Herrera</h4>
                        <p>
                            Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.
                        </p>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-review">
                        <h4>Andre Gonzalez</h4>
                        <p>
                            Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.
                        </p>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-review">
                        <h4>Jon Banks</h4>
                        <p>
                            Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.
                        </p>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-review">
                        <h4>Landon Houston</h4>
                        <p>
                            Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.
                        </p>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-review">
                        <h4>Nelle Wade</h4>
                        <p>
                            Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker.
                        </p>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End reviews Area -->

    <!-- start footer Area -->
    <footer class="footer-area section-gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Quick links</h6>
                        <ul>
                            <li><a href="#">Foydalanish qoidalari</a></li>
                            <li><a href="#">Takliflar</a></li>
                            <li><a href="#">Shikoyatlar</a></li>
                            <li><a href="#">Hamkorlarimiz</a></li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Features</h6>
                        <ul>
                            <li><a href="#">Jobs</a></li>
                            <li><a href="#">Brand Assets</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Terms of Service</a></li>
                        </ul>
                    </div>
                </div> -->
                <!-- <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Resources</h6>
                        <ul>
                            <li><a href="#">Guides</a></li>
                            <li><a href="#">Research</a></li>
                            <li><a href="#">Experts</a></li>
                            <li><a href="#">Agencies</a></li>
                        </ul>
                    </div>
                </div> -->
                <div class="col-lg-2 col-md-6 col-sm-6 social-widget">
                    <div class="single-footer-widget">
                        <h6>Ijtimoiy tarmoqlarda</h6>
                        <p>Yangiliklar tez orada</p>
                        <div class="footer-social d-flex align-items-center">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-dribbble"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-lg-4  col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Newsletter</h6>
                        <p>Stay update with our latest</p>
                        <div class="" id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="form-inline">
                                <input class="form-control" name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">
                                <button class="click-btn btn btn-default"><span class="lnr lnr-arrow-right"></span></button>
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>

                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div> -->
                <p class="mt-80 mx-auto footer-text col-lg-12">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
        </div>
        <img class="footer-bottom" src="template/img/footer-bottom.png" alt="">
    </footer>
    <!-- End footer Area -->

@endsection
