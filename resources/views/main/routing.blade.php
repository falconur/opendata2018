@extends('master.master')
@section("main")
    <header id="header">
        {{--<div class="header-top">--}}
        {{--</div>--}}
        <div class="container-fluid main-menu">
            <div class="row align-items-center justify-content-between d-flex">
                <a href="index.html"><img src="template/img/logo.png" alt="" title="" /></a>
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li class="menu-active"><a href="index.html">Home</a></li>
                        <li><a href="about.html">About</a></li>
                        <li><a href="service.html">Services</a></li>
                        <li><a href="gallery.html">Gallery</a></li>
                        <li><a href="elements.html">Elements</a></li>
                        <li><a href="contact.html">Contact</a></li>
                        <li class="menu-has-children"><a href=""><span class="fa fa-user"></span> Profile</a>
                            <ul>
                                <li><a href="blog-home.html">Blog Home</a></li>
                                <li><a href="blog-single.html">Blog Single</a></li>
                                <li class="menu-has-children"><a href="">Level 2</a>
                                    <ul>
                                        <li><a href="#">Item One</a></li>
                                        <li><a href="#">Item Two</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav><!-- #nav-menu-container -->
            </div>
        </div>
    </header><!-- #header -->
   <div class="pro">

       <div class="row">
           <div class="col-75">
               <div class="container">
                   <form action="{{route('tracks.store')}}" method="POST">
                       @csrf
                       <div class="row">
                           <div class="col-50">
                               <h3>Ma'lumot kiriting</h3>
                               <label for="date"><i class="fa fa-clock-o"></i><i class="fa fa-arrow-circle-o-right"></i> Jo'nash vaqtni kiriting?</label>
                               <input type="time" id="date" name="time_from" placeholder="Jo'nash vaqtini kiritng..">
                               <label for="date2"><i class="fa fa-arrow-circle-o-right"></i><i class="fa fa-clock-o"></i>Yetib borish vaqtni kiriting? </label>
                               <input type="time" id="date2" name="time_to" placeholder="Yetib borish vaqti...">
                               <label for="adr"><i class="fa fa-clock-o"></i> Kechikish vaqt oralig'i (minut hisobida)</label>
                               <input type="number" id="adr" name="time_interval" placeholder="Kechikish vaqt orali'gi">
                               <div class="col-75">
                                   <label>
                                       <input type="checkbox"  name="days[]" value="1"> Dushanba
                                   </label>
                                   <label>
                                       <input type="checkbox"  name="days[]" value="2"> Seshanba
                                   </label>
                                   <label>
                                       <input type="checkbox"  name="days[]" value="3"> Chorshanba
                                   </label>
                                   <label>
                                       <input type="checkbox"  name="days[]" value="4"> Payshanba
                                   </label>
                                   <label>
                                       <input type="checkbox"  name="days[]" value="5"> Juma
                                   </label>
                                   <label>
                                       <input type="checkbox"  name="days[]" value="6"> Shanba
                                   </label>
                                   <label>
                                       <input type="checkbox"  name="days[]" value="7"> Yakshanba
                                   </label>

                               </div>

                               <div class="row">
                                   <div class="col-50">
                                       <label for="state">Yo'ldoshingizni tanlang?</label>
                                       <select name="partner">
                                           <option value="1">Erkak</option>
                                           <option value="2">Ayol</option>
                                           <option value="3">Bola</option>
                                       </select>
                                   </div>
                                   <div class="col-50">
                                       <label for="zip">Summani kiriting?</label>
                                       <input type="number" id="zip" name="price" placeholder="So'mdagi narx...">
                                   </div>
                                   <input type="hidden" id="location_from_lat" name="location_from_lat" value="0">
                                   <input type="hidden" id="location_from_long" name="location_from_long" value="6">
                                   <input type="hidden" id="location_to_lat" name="location_to_lat" value="7" novalidate>
                                   <input type="hidden" id="location_to_long" name="location_to_long" value="9" novalidate>
                               </div>
                           </div>
                       </div>

                       <input type="submit" value="Continue to checkout" class="btn">
                   </form>
               </div>
           </div>


       </div>
       <div class="map" id="mapid">

       </div>
   </div>






    <!-- start footer Area -->
    <footer class="footer-area section-gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Quick links</h6>
                        <ul>
                            <li><a href="#">Jobs</a></li>
                            <li><a href="#">Brand Assets</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Terms of Service</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Features</h6>
                        <ul>
                            <li><a href="#">Jobs</a></li>
                            <li><a href="#">Brand Assets</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Terms of Service</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Resources</h6>
                        <ul>
                            <li><a href="#">Guides</a></li>
                            <li><a href="#">Research</a></li>
                            <li><a href="#">Experts</a></li>
                            <li><a href="#">Agencies</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6 social-widget">
                    <div class="single-footer-widget">
                        <h6>Follow Us</h6>
                        <p>Let us be social</p>
                        <div class="footer-social d-flex align-items-center">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-dribbble"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4  col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Newsletter</h6>
                        <p>Stay update with our latest</p>
                        <div class="" id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="form-inline">
                                <input class="form-control" name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">
                                <button class="click-btn btn btn-default"><span class="lnr lnr-arrow-right"></span></button>
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>

                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <p class="mt-80 mx-auto footer-text col-lg-12">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            </div>
        </div>
        <img class="footer-bottom" src="img/footer-bottom.png" alt="">
    </footer>
    <!-- End footer Area -->

@endsection