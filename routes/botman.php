<?php
use App\Http\Controllers\BotManController;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use Botman\Botman\Drivers\DriverManager;
use BotMan\BotMan\Messages\Attachments\Location;



$botman = resolve('botman');

$botman->hears('/start', function (BotMan $bot){
    $bot->startConversation(new \App\Conversations\StartConversation());
});


//$botman->hears('Start conversation', BotManController::class.'@startConversation');

//$botman->hears('register', function (BotMan $bot) {
//    $bot->startConversation(new \App\Conversations\RegisterConversation());
//});