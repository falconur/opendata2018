<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});


Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');

Route::get('/index', function(){
    return view('main.index');
});
Route::get('/routing', function (){
    return view('main.routing');
})->name('main.routing');

Route::resource('/tracks', 'TrackController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

